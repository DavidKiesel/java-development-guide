Contents:
    - Introduction
    - Get Help
        - Maven Help
            - Maven Resources
                - Maven pom.xml Reference
                - Maven Life Cycle
                - Maven Super POM
                - Search for Artifacts
            - Maven Plugin Help
            - Maven Expression Evaluation
            - Maven Dependency Troubleshooting
        - git Help
        - Java Help
    - Configure Settings
        - Configure Maven
            - ~/.m2/settings.xml
            - ~/.m2/settings-security.xml
    - Create a Maven Project
        - Find a Maven Archetype
        - Create an Application
        - Create a Servlet
        - Create a Maven Archetype
    - Begin Source Code Management
        - Initialize Git
        - Add a Git Remote
        - Basic Git Commands
    - Add Code
        - Javadoc
        - Resources
            - Unfiltered Resources
            - Filtered Resources
            - Load a Resource
        - Logging
            - Logging with slf4j and log4j2
        - Tests
            - Unit Tests with junit 5
            - Integration Tests
            - Mocks
            - HTTP Server Mocks
            - Kafka Server Mocks
        - Exception Handling
            - Handle Exceptions in a Static Block
            - Handle Exceptions in toString()
            - Load Stack Trace to a String
        - SSL
        - Java Data Wrangling
            - Initialize an Array
                - Initialize a String Array
                - Initialize an int Array
            - Initialize a List
                - Initialize a List<String>
                - Initialize a List<Integer>
            - Initialize a Map<K,V>
            - String Concatenation
            - Type Conversions
                - String to Byte Array
                - Byte Array to String
                - String to LocalDate
                - LocalDate to String
                - String to com.google.protobuf.Timestamp
                - com.google.protobuf.Timestamp to String
                - Byte Array to com.google.protobuf.ByteString
                - com.google.protobuf.ByteString to Byte Array
                - String Array to List<String>
                - List<String> to String Array
                - int Array to List<Integer>
                - List<Integer> to int Array
            - JSON and Java Objects
            - URLs and URIs
    - Build and Deploy Code
        - Build a Package
        - Generate Javadoc
        - Execute Unit Tests
        - Execute Integration Tests
        - Report Test Coverage
        - Generate a Source Artifact
        - Generate a Javadoc Artifact
        - Checksum Artifacts
        - Sign Artifacts
        - Install Artifacts Locally
        - Deploy Snapshot Artifacts
        - Deploy Release Artifacts

